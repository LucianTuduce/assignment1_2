package com.fortech.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.ejb.Stateless;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

@SuppressWarnings("deprecation")
@Stateless
public class QueryTimeHelper {

	public String getLocalTime(String lat, String longit) throws ClientProtocolException, IOException {
		@SuppressWarnings("resource")
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet("http://api.geonames.org/timezone?lat=" + lat + "&lng=" + longit + "&username=demo");
		HttpResponse response = client.execute(request);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		String result = null;
		while ((line = rd.readLine()) != null) {
			if (line.contains("<time>"))
				result = line;
		}

		return result;
	}

}
