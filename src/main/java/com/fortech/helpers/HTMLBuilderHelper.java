package com.fortech.helpers;

import java.util.List;

import org.utcn.model.Flight;

public class HTMLBuilderHelper {

	public static String getHTMLFormatTableString(List<Flight> flights){
		String htmlBuild = "";
		
		for (Flight flightAdded : flights) {
			htmlBuild = htmlBuild + "<tr>";
			htmlBuild = htmlBuild + "<td>" + flightAdded.getId() + "</td>";
			htmlBuild = htmlBuild + "<td>" + flightAdded.getAirplaneType() + "</td>";
			htmlBuild = htmlBuild + "<td>" + flightAdded.getDepartureCity() + "</td>";
			htmlBuild = htmlBuild + "<td>" + flightAdded.getDepartureDate() + "</td>";
			htmlBuild = htmlBuild + "<td>" + flightAdded.getDepartureHour() + "</td>";
			htmlBuild = htmlBuild + "<td>" + flightAdded.getArrivalCity() + "</td>";
			htmlBuild = htmlBuild + "<td>" + flightAdded.getArrivalDate() + "</td>";
			htmlBuild = htmlBuild + "<td>" + flightAdded.getArrivalHour() + "</td>";
			htmlBuild = htmlBuild + "</tr>";
		}
		return htmlBuild;
	}
}
