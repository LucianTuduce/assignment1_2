package org.utcn.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.utcn.service.FlightService;

import com.fortech.helpers.FileReadHelper;
import com.fortech.helpers.HTMLBuilderHelper;

/**
 * Servlet implementation class DeleteFlightServlet
 */
@WebServlet("/DeleteFlightServlet")
@Stateless
public class DeleteFlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String ADMIN_DELETE_FLIGHT_TOP_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/adminloggedFirstHalf.html";
	private static final String ADMIN_DELETE_FLIGHT_BOTTOM_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/adminloggedSecondHalf.html";
	
	@EJB
	private FlightService flightService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteFlightServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter loginWritter = response.getWriter();
		String htmlBuild = "";
		String htmlFirstHalfOfPage = "";
		String htmlSecondHalfOfPage = "";
		String id = request.getParameter("deletionFlight");

		flightService.delete(Integer.parseInt(id));
		htmlBuild = HTMLBuilderHelper.getHTMLFormatTableString(flightService.getFlights());

		htmlFirstHalfOfPage = FileReadHelper.readDataFromFile(ADMIN_DELETE_FLIGHT_TOP_HTML);
		htmlSecondHalfOfPage = FileReadHelper.readDataFromFile(ADMIN_DELETE_FLIGHT_BOTTOM_HTML);
		loginWritter.println(htmlFirstHalfOfPage + htmlBuild
				+ htmlSecondHalfOfPage);
	}
}
