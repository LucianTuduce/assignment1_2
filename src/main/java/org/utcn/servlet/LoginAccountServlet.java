package org.utcn.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.utcn.model.Flight;
import org.utcn.model.User;
import org.utcn.service.CityService;
import org.utcn.service.FlightService;
import org.utcn.service.UserService;

import com.fortech.helpers.FileReadHelper;
import com.fortech.helpers.HTMLBuilderHelper;

/**
 * Servlet implementation class LoginAccountServlet
 */
@WebServlet("/LoginAccountServlet")
@Stateless
public class LoginAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String USERNAME = "admin";
	private static final String PASSWORD = "admin";
	private static final String ADMIN_LOGGED_TOP_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/adminloggedFirstHalf.html";
	private static final String ADMIN_LOGGED_BOTTOM_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/adminloggedSecondHalf.html";
	private static final String USER_LOGGED_TOP_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/userloggedFirstHalf.html";
	private static final String USER_LOGGED_BOTTOM_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/userloggedSecondHalf.html";

	@EJB
	private UserService userService;

	@EJB
	private FlightService flightService;

	@EJB
	private CityService cityService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginAccountServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		PrintWriter loginWritter = response.getWriter();

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String htmlBuild = "";
		String htmlFirstHalfOfPage = "";
		String htmlSecondHalfOfPage = "";
		User user = userService.getUser(username, password);
		List<Flight> flights = flightService.getFlights();
		//List<City> cities = cityService.getCities();

		if (user == null) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/login.html");
			PrintWriter out = response.getWriter();
			out.println("<font color=red>Failed to authenticate user</font>");
			rd.include(request, response);
		}

		if (user.getUsername().equals(USERNAME)	&& user.getPassword().equals(PASSWORD)) {

			Cookie userName = new Cookie("user", username);
			userName.setMaxAge(10 * 60);
			HttpSession session = request.getSession();
			session.setAttribute("user", "Admin");
			session.setMaxInactiveInterval(10 * 60);
			response.addCookie(userName);
			
			htmlBuild = HTMLBuilderHelper.getHTMLFormatTableString(flights);
			
			htmlFirstHalfOfPage = FileReadHelper.readDataFromFile(ADMIN_LOGGED_TOP_HTML);
			htmlSecondHalfOfPage = FileReadHelper.readDataFromFile(ADMIN_LOGGED_BOTTOM_HTML);
			loginWritter.println(htmlFirstHalfOfPage+htmlBuild+htmlSecondHalfOfPage);
		} else if (user != null) {

			Cookie userName = new Cookie("user", username);
			userName.setMaxAge(30 * 60);
			response.addCookie(userName);
			HttpSession session = request.getSession();
			session.setAttribute("user", "Client");
			session.setMaxInactiveInterval(10 * 60);
			htmlBuild = HTMLBuilderHelper.getHTMLFormatTableString(flights);
			
			htmlFirstHalfOfPage = FileReadHelper.readDataFromFile(USER_LOGGED_TOP_HTML);
			htmlSecondHalfOfPage = FileReadHelper.readDataFromFile(USER_LOGGED_BOTTOM_HTML);
			loginWritter.println(htmlFirstHalfOfPage+htmlBuild+htmlSecondHalfOfPage);
		}
	}
}
