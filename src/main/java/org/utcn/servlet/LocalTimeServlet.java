package org.utcn.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.utcn.model.City;
import org.utcn.service.CityService;
import org.utcn.service.FlightService;

import com.fortech.helpers.FileReadHelper;
import com.fortech.helpers.HTMLBuilderHelper;
import com.fortech.helpers.QueryTimeHelper;

/**
 * Servlet implementation class LocalTimeServlet
 */
@WebServlet("/LocalTimeServlet")
@Stateless
public class LocalTimeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String USER_LOGGED_TOP_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/userloggedFirstHalf.html";
	private static final String USER_LOGGED_MIDDLE_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/userloggedSecondHalf.html";
	private static final String USER_LOGGED_BOTTOM_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/userloggedThirdHalf.html";
	
	@EJB
	private CityService cityService;
	
	@EJB
	private FlightService flightService;

	@EJB
	private QueryTimeHelper helper;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LocalTimeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String orasSelectat = request.getParameter("city");
		City city = cityService.getCity(orasSelectat);
		String latitude = city.getLat();
		String longitude = city.getLongit();
		String dateAndTime = null;
		String htmlBuild = "";
		String htmlFirstHalfOfPage = "";
		String htmlSecondHalfOfPage = "";
		String htmlThirdHalfOfPage = "";
		
		PrintWriter out = response.getWriter();

		if (latitude != null && longitude != null) {

			try {
				dateAndTime = helper.getLocalTime(latitude, longitude);
				String timeInfo = city.getName() + " and the date and time is " + dateAndTime;
				htmlBuild = HTMLBuilderHelper.getHTMLFormatTableString(flightService.getFlights());
				
				htmlFirstHalfOfPage = FileReadHelper.readDataFromFile(USER_LOGGED_TOP_HTML);
				htmlSecondHalfOfPage = FileReadHelper.readDataFromFile(USER_LOGGED_MIDDLE_HTML);
				htmlThirdHalfOfPage = FileReadHelper.readDataFromFile(USER_LOGGED_BOTTOM_HTML);
				out.println(htmlFirstHalfOfPage+htmlBuild+htmlSecondHalfOfPage+ timeInfo + htmlThirdHalfOfPage);
			} catch (Exception e) {

			}
		}
	}

}
