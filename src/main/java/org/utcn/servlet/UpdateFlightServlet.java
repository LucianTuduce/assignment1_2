package org.utcn.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.utcn.model.Flight;
import org.utcn.service.FlightService;

import com.fortech.helpers.FileReadHelper;
import com.fortech.helpers.HTMLBuilderHelper;

/**
 * Servlet implementation class UpdateFlightServlet
 */
@WebServlet("/UpdateFlightServlet")
@Stateless
public class UpdateFlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private FlightService flightService;

	private static final String ADMIN_UPDATE_FLIGHT_TOP_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/adminloggedFirstHalf.html";
	private static final String ADMIN_UPDATE_FLIGHT_BOTTOM_HTML = "D:/DS/A1_2/assignment1_2/src/main/webapp/adminloggedSecondHalf.html";
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateFlightServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter loginWritter = response.getWriter();
		String htmlBuild = "";
		String htmlFirstHalfOfPage = "";
		String htmlSecondHalfOfPage = "";

		String flightNumber = request.getParameter("airplaneFlight");
		String airplaneType = request.getParameter("airplaneType");
		String departureCity = request.getParameter("departureCity");
		String departureDate = request.getParameter("departureDate");
		String departureHour = request.getParameter("departureHour");
		String arrivalCity = request.getParameter("arrivingCity");
		String arrivalDate = request.getParameter("arrivingDate");
		String arrivalHour = request.getParameter("arrivingHour");

		Flight flight = flightService.getOne(Integer.parseInt(flightNumber));

		if(airplaneType.equals("")){
			airplaneType = flight.getAirplaneType();
		}else {
			airplaneType = request.getParameter("airplaneType");
		}
		
		if(departureCity.equals("")){
			departureCity = flight.getDepartureCity();
		}else{
			departureCity = request.getParameter("departureCity");
		}
		
		if(departureDate.equals("")){
			departureDate = flight.getDepartureDate();
		}else{
			departureDate = request.getParameter("departureDate");
		}
		
		if(departureHour.equals("")){
			departureHour = flight.getDepartureHour();
		}else {
			departureHour = request.getParameter("departureHour");
		}
		
		if(arrivalCity.equals("")){
			arrivalCity = flight.getArrivalCity();
		}else{
			arrivalCity = request.getParameter("arrivingCity");
		}
		
		if(arrivalDate.equals("")){
			arrivalDate = flight.getArrivalDate();
		}else{
			arrivalDate = request.getParameter("arrivingDate");
		}
		
		if(arrivalHour.equals("")){
			arrivalHour = flight.getArrivalHour();
		}else{
			arrivalHour = request.getParameter("arrivingHour");
		}
		
		flight.setAirplaneType(airplaneType);
		flight.setDepartureCity(departureCity);
		flight.setDepartureDate(departureDate);
		flight.setDepartureHour(departureHour);
		flight.setArrivalCity(arrivalCity);
		flight.setArrivalDate(arrivalDate);
		flight.setArrivalHour(arrivalHour);

		
		flightService.update(flight);
		htmlBuild = HTMLBuilderHelper.getHTMLFormatTableString(flightService.getFlights());
		
		htmlFirstHalfOfPage = FileReadHelper.readDataFromFile(ADMIN_UPDATE_FLIGHT_TOP_HTML);
		htmlSecondHalfOfPage = FileReadHelper.readDataFromFile(ADMIN_UPDATE_FLIGHT_BOTTOM_HTML);
		loginWritter.println(htmlFirstHalfOfPage + htmlBuild
				+ htmlSecondHalfOfPage);
	}
}
