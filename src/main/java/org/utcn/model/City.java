package org.utcn.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the city database table.
 * 
 */
@Entity
@NamedQuery(name="City.findAll", query="SELECT c FROM City c")
public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idcity;

	private String lat;

	private String longit;

	private String name;

	public City() {
	}

	public int getIdcity() {
		return this.idcity;
	}

	public void setIdcity(int idcity) {
		this.idcity = idcity;
	}

	public String getLat() {
		return this.lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLongit() {
		return this.longit;
	}

	public void setLongit(String longit) {
		this.longit = longit;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}