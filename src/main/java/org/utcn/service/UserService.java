package org.utcn.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.utcn.model.User;

@Stateless
public class UserService {

	@PersistenceContext
	private EntityManager entityManager;

	public User getUser(String username, String password) {
		TypedQuery<User> typeQuery = entityManager.createQuery("SELECT u FROM User u", User.class);
		List<User> users = typeQuery.getResultList();		
		for(User user: users){
			if(user.getUsername().equals(username) && user.getPassword().equals(password)){
				return user;
			}
		}	
		return null;
	}

}
