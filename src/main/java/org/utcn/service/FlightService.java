package org.utcn.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.utcn.model.Flight;

@Stateless
public class FlightService {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void insert(Flight flight){
		entityManager.persist(flight);
	}
	
	public void update(Flight flight){
		entityManager.merge(flight);
	}
	
	public void delete(int id){
		entityManager.remove(entityManager.find(Flight.class, id));
	}
	
	public Flight getOne(int id){
		return entityManager.find(Flight.class, id);
	}
	
	public List<Flight> getFlights(){
		TypedQuery<Flight> flightsQuery = entityManager.createQuery("SELECT f FROM Flight f", Flight.class);
		return flightsQuery.getResultList();
	}
}
