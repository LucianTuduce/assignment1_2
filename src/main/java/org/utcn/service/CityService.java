package org.utcn.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.utcn.model.City;

@Stateless
public class CityService {

	@PersistenceContext
	private EntityManager entityManager;
	
	public List<City> getCities(){
		TypedQuery<City> citiesQuery = entityManager.createQuery("SELECT c FROM City c", City.class);
		return citiesQuery.getResultList();
	}
	
	public City getCity(String name){
		TypedQuery<City> citiesQuery = entityManager.createQuery("SELECT c FROM City c", City.class);
		for(City city: citiesQuery.getResultList()){
			if(city.getName().equals(name)){
				return city;
			}
		}
		return null;
	}
}
